# 《C语言程序设计》_The C Programming Language_

## 参考文献Bibliographies

1. [美]布莱恩· W.克尼汉(Brian W. Kernighan),丹尼斯· M.里奇(Dennis M.Ritchie) 著, 徐宝文 李志 译. C程序设计语言（第2版·新版）典藏版[M]. 北京: 机械工业出版社. 2019-03-22. ISBN: 978-7-111-61794-5.
1. [美]克洛维斯· L.汤多(Clovis L. Tondo) 斯科特· E.吉姆佩尔(Scott E. Gimpel)著. 杨涛 等译. C程序设计语言（第2版·新版）习题解答（典藏版）[M]. 北京: 机械工业出版社. 2019-03-22. ISBN: 978-7-111-61901-7.
1. [美]Brian W.Kernighan,Dennis M.Ritchie. C程序设计语言（英文版·第2版）[M]. 北京: 机械工业出版社. 2005-07-01. ISBN: 7-111-19626-0.
