---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "__"
footer: "@aRoming"
math: katex
---

<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# A Tutorial Introduction导言

---

## Table of Contents

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Table of Contents](#table-of-contents)
2. [Getting Started](#getting-started)
3. [Basic Syntax](#basic-syntax)
4. [Advanced Syntax](#advanced-syntax)

<!-- /code_chunk_output -->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Getting Started

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### hello world

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Basic Syntax

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Variables and Arithmetic Expressions

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### The for Statement

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Symbolic Constants

---

1. 字面量/幻数
    + 难于提供有效信息
    + 修改困难（一致性）
1. `#define符号常量`：指令行末尾没有分号
1. `const常量`(C90)
1. `符号常量`和`常量`通常使用全大写形式

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Character Input and Output

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Advanced Syntax

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Arrays

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Functions

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Arguments - Call by Value

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Character Arrays

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### External Variables and Scope

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

**:ok: End of This Slide :ok:**
