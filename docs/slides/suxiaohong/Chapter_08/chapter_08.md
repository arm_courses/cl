---
presentation:
    minScale: 0.1
    maxScale: 1.0
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom 
---

<!-- slide -->
# 第08章：数组Array

<!-- slide -->
# 本章学习内容

1. 数组类型，数组的定义和初始化，以及对数组名特殊含义的理解
1. 向函数传递一维数组和二维数组（多维数组）
1. 排序、查找、最大最小值等常用算法

<!-- slide -->
# :question:为什么引入数组Array

## 需要经常批量处理相同类型的变量

## Array是一组具有相同数据类型的变量的集合

<!-- slide -->
# :question:什么是语法糖

# :question:为什么C有指针还需要有数组

<!-- slide -->
# 一维数组的定义

```c{.line-numbers}
<store_type> <data_type> <array_name> [<LEN>];
```

1. LEN须为正整型的`literal`或`const`，不能是`variable`（C99可以是`variable`，本课程以C89/C90为标准）
1. LEN缺省的情况，可由初始化列表数量推导出

## :question:为什么`const`可以，而`variable`不可以

<!--  slide -->
# 一维数组的定义（续）

## ❗️ 数组的内存空间由编译器在编译时分配

> :point_right: **`const`存储于`.rodata`** :book: P35，**定义时必须初始化**
>
> :point_right: **`variable`** 在 **编译时数值无法确定**

<!-- slide -->
# 一维数组的初始化:book: P191

1. 当数组是自动变量时，其元素初始值是随机数
1. 当数组是静态变量或全局变量时，其元素初始值是0

```c{.line-numbers}
int a[5] = { 12, 34, 56 ,78 ,9 };
int a[5] = { 0 };
int a[] = { 11, 22, 33, 44, 55 };
```

<!-- slide -->
# 数组元素访问

```c{.line-numbers}
<array_name> [subscript];
```

1. `subscript`可以是`literal`、`const`、`variable`、`expression`
1. ⚠️ $ subscript \in [0 , LEN-1]$
    + 越界下标访问即访问“不确定”的内存，**结果未定义**
    + 编译器可能无法检查是否越界访问（变量或表达式下标运行时才能确定）

<!-- slide -->
# 越界下标访问📖 P192

```c{.line-numbers}
int a = 1, c = 2, b[5] = {0}, i;
printf("%p, %p, %p\n", b, &c, &a);
//%p is pointer
for (i=0; i<=8; i++)
{
    b[i] = i;
    printf("%d  ", b[i]);
}
printf("\nc=%d, a=%d, i=%d\n", c, a, i);
return 0;
```

<!-- slide -->
# 越界下标访问

```c{.line-numbers}
int c = 0;
int a[] = { 1, 2, 3, 4, 5 };
int b = 6,d=7;
printf("%d", a[5]);     //access beyond index
//in Mac, output is 32766
```

<!-- slide -->
# 一维数组的使用例子📖 P190

```c{.line-numbers}
int score[5],totalScore=0,i;
printf("Input the scores of then students:\n");

for(i=0;i<5;++i)
{
    scanf("%d",&score[i]);
    totalScore += score[i];
}
```

<!-- slide -->
# 二维数组的定义:book:P193

```c{.line-numbers}
$(store_type) $(data_type) $(array_name) [$(LEN_01)][$(LEN_02)];
```

1. `LEN_01`（高维）指定行数、`LEN_02`（低维）指定列数（:book: P193描述有点晦涩）
1. 二维数组存储顺序：按行存储（低维优先），即先存储低维、再存储高维

<!-- slide -->
# 数组的内存空间:book:P194

```c{.line-numbers}
LEN_01 × LEN_02 × ... × sizeof(data_type)
```

<!-- slide -->
# 二维数组的初始化

## :book:P195数组第二维的长度声明永远都不能省略

## :question:二维以上的数组呢 ➡ :exclamation:第一维不可省略

<!-- slide -->
# 8.3向函数传递一维数组📖P197

1. 数组名代表数组第一个元素的地址（数组首地址）
1. 向函数传递一维数组时传递数组首地址 📖P198，**此时Array退化成Pointer**（丢失LEN信息），因此，传递数组时一般需额外传递一个整型的LEN信息:book:P197例8.5 :book:P199
1. 由于传递的是指针，在被调函数内可以修改数组元素

## :question:为什么C语言这样设计

## :question:数组的LEN信息存储在哪里

<!-- slide -->
# :point_right: 为什么C语言这样设计

> :book:P198之所以这样是出于性能方面的考虑，因为相对于以传值方式将全部数组元素的副本传给被调函数而言，只复制一个地址值的效率自然要高得多。

## :question:  是否还有其他原因，待进一步寻找答案

<!-- slide -->

# :point_right: 数组的LEN信息存储在哪里

存储于编译器编译过程中的符号表中

<!-- slide -->
# :book:P197 8.3 向函数传递一维数组

<!-- slide -->
# 基础类型参数传递

![normal_variable_as_argument](./00_Images/normal_variable_as_argument.png)

<!-- slide -->
# 数组类型参数传递

![array_as_argument](./00_Images/array_as_argument.png)

<!-- slide -->
# 传递数组与传递指针

```c{.line-numbers}
int Average(int score[],int n);
int Average(int *score,int n);
```

<!--  slide -->

# :book:P197 - P198 例8.5

```c{.line-numbers}
//Average()函数改写成如下哪种更好？

if(n <= 0) return -1;

return n > 0 ? sum/n : -1;
```

<!-- slide -->
# :book:P199例8.6

```c{.line-numbers}
# define N 40
...
int ReadScore(int score[]){
    int i=-1;
    do{
        i++;
        printf("Input score:");
        scanf("%d",&score[i]);
    }while(score[i]>=0);
    return i;
}
```

<!-- slide -->
# :book:P199 - P200例8.6（改写）

```c{.line-numbers}

int main(void){
    const MAX_STU=40;
    int score[MAX_STU],aver,n;
    n=read_score(score,MAX_STU);
    ...
}

int read_score(int score[],int max_size){
    int i=0,tmp_score=-1;
    for(i=0;i<max_size;++i)
    {
        printf("Input score:");
        scanf("%d",&tmp_score);
        if(tmp_score<0){
            break;
        }
        score[i]=tmp_score;
    }
    return i;
}
```

<!-- slide -->
# :book:P200 - P201 例8.7

```c{.line-numbers}
//line 04
int FindMax(int score[], int n);
//line 10
max = FindMax(score,n);
```

<!-- slide -->
# :book:P200 - P201 例8.7（续）

```c{.line-numbers}
//line 26
int FindMax(int score[], int n){
...
max = score[0];
for(i = 1;i < n;++i){
    if(score[i] > max) max = score[i];
}
...
}
```

<!-- slide -->
# 语法糖

>**语法糖（Syntactic sugar）**，也译为糖衣语法，是由英国计算机科学家彼得·兰丁发明的一个术语，指计算机语言中添加的某种语法，**这种语法对语言的功能没有影响，但是更方便程序员使用**。
>--Wikipedia

<!-- slide -->
# 语法糖（续）

## :+1:  编程简单、程序简洁、可读性高、接近自然语言

## :-1: 学习成本高、需猜测被添加了什么、BUG较难调试

<!-- slide -->
# 📖P202 8.4排序和查找

1. Sorting排序是指将一系列无序的数据按照特定的顺序重新排列为有序的数据的过程
1. Searching查找是指在一系列数据中查找特定元素的过程
1. Sorting通常是为了Searching时效率更高

<!-- slide -->
# 交换法排序Exchange Sort

1. 交换法排序借鉴了求最大值（或最小值）的思想
1. 交换法排序每次循环总是将最大值（或最小值）交换至当前循环位
1. 交换法排序也称为冒泡排序Bubble Sort
1. 交换法排序相当于算法世界里面的`Hello World`

<!-- slide -->
# 交换法排序Exchange Sort（续）

```c{.line-numbers}
for(i = 0; i < n-1; ++i){
    for(j = i+1; j<n; ++j ){
        if(a[j] > a[i]){
            swap(a[i],a[j]);
        }//if
    }//for j
}//for i
```

<!-- slide -->
# 📖P205 两数交换swap

```c{.line-numbers}
temp=a[j];
a[j]=a[i];
a[i]=temp;
```

:question:  :book:P205 是否可以不引入临时变量实现“两数交换”

<!-- slide -->
# 📖P205 两数交换swap（续）

```c{.line-numbers}
lhs = lhs + rhs;
rhs = lhs - rhs;
lhs = lhs - rhs;
```

```c{.line-numbers}
lhs = lhs ^ rhs;
rhs = lhs ^ rhs;
lhs = lhs ^ rhs;
```

<!-- slide -->
# 📖P205 两数交换swap（再续）

```c{.line-numbers}
void swap_pointer_with_bitwise(unsigned long *lhs, \
                            unsigned long *rhs){
    *lhs = *lhs ^ *rhs;
    *rhs = *rhs ^ *lhs;
    *lhs = *lhs ^ *rhs;
}
char *first = "here";
char *second = "there";
swap_pointer_with_bitwise((unsigned long *)&first, \
                        (unsigned long *)&second);
```

<!-- slide -->
# 📖P205 选择法排序Selection Sort

```c{.line-numbers}
for (i = 0; i < n-1; i++)  {
    k = i;
    for (j = i + 1; j < n; j++){
        if (score[j] > score[k])
            k=j;//记录此轮比较中最高分的元素下标  k = j;
    }//for j
    //若k中记录的最大数不在位置i，则进行交换
    if(k != i)  swap(score[i], score[k]);
}//for i
```
:question: 选择法相对于交换法的改进有哪些

<!-- slide -->
# 选择法的基本思想

选择排序的基本思想：

1. 第0趟，在待排序记录r[0] ~ r[n-1]中选出最大（或最小）的记录，将它与r[0]交换
1. 第1趟，在待排序记录r[1] ~ r[n-1]中选出最大（或最小）的记录，将它与r[1]交换
1. 第i趟，在待排序记录r[i] ~ r[n-1]中选出最小的记录，将它与r[i]交换，使有序序列不断增长直到全部排序完毕

<!-- slide -->
# 选择法相对于交换法的改进

每趟排序过程中，仅选择（记录下）最大（或最小）的下标，在本趟排序结束前进行交换（或不交换），避免一趟排序过程中无价值的交换

<!-- slide -->
# :book: P209 线性查找Linear Search

1. 线性查找也称为顺序查找Sequential Search
1. :+1: 算法简单直观，不必要求事先有序
1. :-1: 效率较低

<!-- slide -->
# :book: P210 例8.10

```c{.line-numbers}
//line 32
int  LinSearch(long num[], long x, int n){
    int i;
    for (i = 0; i < n; i++) {
        if (num[i] == x){
            return i;
        }
    }
    return -1;
}
```

<!--  slide -->
# 折半查找Binary Search

1. 折半查找也称为对分搜索
1. 折半查找要求事先有序

<!--  slide -->
# 折半查找的基本思想

1. 选取位于数组中间的元素与待查找键比较
1. 如果相等，待查找键找到，返回下标
1. 否则，将查找区间缩小至原来一半继续查找

<!-- slide -->
# 折半查找

```c{.line-numbers}
low = 0;high = n - 1;
while (low <= high)
{
    mid = (high + low) / 2;
    if (x > num[mid]){
        low = mid + 1;//search in (mid, high]
   	}else  if (x < num[mid])
    {
        high = mid - 1;//search in [low, mid)
   	}else  
    {
        return mid;//find it...
   	}
}
return -1;//can not find...
```

<!-- slide -->
# :book: P213

```c{.line-numbers}
void demo_array(int a[3], int len){
    int i=0;
    /*:warning: Sizeof on array function parameter
    will return size of 'int *' instead of 'int [3]*/
    printf("%lu\t",sizeof(a));
    for(;i<len;++i){
        printf("%d\t",a[i]);
    }
}
int an[9]={1,2,3,4,5,6,7,8,9};
demo_array(an,9);
//output ==> 8	1	2	3	4	5	6	7	8	9	
```

<!-- slide -->
# :book: P213（续）

1. 一维数组形参没必要显性数组长度，应该用另一个整型形参传递数组长度
1. 实参和形参没必要同名

<!-- slide -->
# 8.5向函数传递多维数组:book: P214

1. 数组名作为实参，传递数组首地址
1. 形参除最高维（第一维）外，其他维长度需声明

```c{.line-numbers}
<data_type> a[M][N];
a[i][j] --> a [i * N +j] --> a + ( i * N + j )
```

## 使用`a[i][j]`时需要编译器帮忙计算偏移

<!-- slide -->
# 例子

```c{.line-numbers}
    int a[][3]={1,2,3,4,5,6,7,8,9};
    /*warning: Incompatible pointer types initializing 'int *'
    with an expression of type 'int [3][3]'*/
    int *pa=a;
    pa=*a;//no warning here
    pa=a[0];
    int (*paa)[3]=a;

    printf("%d\n",*(pa+7));
    printf("%d\n",paa[2][1]);
    printf("%d\n",*(*(paa+2)+1));
```

:book: 11.2 指针和二维数组间的关系 P292

<!-- slide -->
# :book: P 214 例8.12

```c{.line-numbers}
//line 05
void AverforStud(int score[][COURSE_N], int sum[], \
                   float aver[], int n) 
//line 11
int score[STUD_N][COURSE_N], sumS[STUD_N], sumC[COURSE_N], n;
long num[STUD_N];
//line 17
AverforStud(score, sumS, averS, n);
```

<!-- slide -->
# Thank You && To Be Continue...

## End of This Chapter