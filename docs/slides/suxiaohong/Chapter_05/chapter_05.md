---
presentation:
    minScale: 0.1
    maxScale: 1.5
    theme: beige.css
    enableSpeakerNotes: true
    slideNumber: true
    transition: zoom
    transitionSpeed: slow
---

<!-- slide -->
# 第五章： 选择控制结构

## 广东财经大学华商学院信息工程系

### aRoming

<!-- slide -->
# 本章内容

+ 算法的描述方法
+ `if`语句
    + `if - else`语句
+ `switch`语句
+ `break`语句
+ 关系运算符
    + 条件运算符
    + 逻辑运算符
+ 程序测试

## 问题求解

1. 分解与分治？并行与串行？
1. 分治：分而治之

## 程序与编程语言

## 算法

1. 命令式语言与声明式语言SQL，程序 = 数据结构 + 算法
1. Knuth ==> 计算机算法先驱、《计算机程序设计艺术》、TeX排版语言
Markdown

### 算法的衡量

1. 有穷性（Finiteness）：算法的有穷性是指算法必须能在执行有限个步骤之后终止；
1. 确切性(Definiteness)：算法的每一步骤必须有确切的定义；
1. 可行性(Effectiveness)：算法中执行的任何计算步骤都是可以被分解为基本的可执行的操作步，即每个计算步都可以在有限时间内完成（也称之为有效性）。
1. 输入项(Input)：一个算法有0个或多个输入，以刻画运算对象的初始情况，所谓0个输入是指算法本身定出了初始条件；
1. 输出项(Output)：一个算法有一个或多个输出，以反映对输入数据加工后的结果。没有输出的算法是毫无意义的；

if(x>=0) xx;
else if(x<=0) xxx;

### 算法的描述方法
1. 自然语言描述： 二义性，形式化语言Forumla
1. 流程图描述
	1. ANSI流程图
	1. NS结构化流程图	 
1. 伪代码 

## 关系运算符与关系表达式
运算符：一种告诉机器执行特定的数学或逻辑操作的符号
++ --

## 分支语句
### 单分支
if(表达式P）
{
	语句A;
	语句B;
}

if(表达式P）
	语句A;
	语句B;



### 多分支

## 条件运算符与条件表达式
三目运算符`Ex1?Ex2:Ex3`

if(disc == 0.0)

0.4转换成二进制

## switch语句

## 逻辑运算符和逻辑表达式
A && B
A || B

&& || !

2 && 15 ==> 1


& | ^ ~ >> << 

Test Case
Use Case UML

1. 明显应该输出假
1. 明显应该输出真
1. 边界真
1. 边界假

<!-- slide -->
# 位运算符Bitwise Operators

1. 位运算符的操作数只能是有符号或无符号的 **整型**
1. 位运算符主要用于 **系统软件编程** 和 **嵌入式软件编程** 等需要实现类似逻辑开关的场景

<!-- slide -->

# 位运算符Bitwise Operators（续）

1. 按位位运算符
    1. `&`: 按位与（Bitwise AND）
    1. `|`: 按位或（Bitwise Inclusive OR）
    1. `^`: 按位异或（Bitwise Exclusive OR）
    1. `~`: 按位取反（Complement）
1. 位移运算符
    1. `<<`: 左移（Left Shift）
    1. `>>`: 右移（Right Shift）

<!-- slide -->
# 按位位运算符

|`a`|`b`|`a&b`|`a|b`|`a^b`|`~a`|
|:--:|:--:|:--:|:--:|:--:|:--:|
|0|0|0|0|0|1|
|0|1|0|1|1|1|
|1|0|0|1|1|0|
|1|1|1|1|0|0|

<!-- slide -->
# 位移运算符

1. **算术位移：** 使用符号位补位，分算术左移和算术右移
1. **逻辑位移：** 使用“0”补位
1. 汇编指令中：
    + `SHL(SHift Left)`逻辑左移
    + `SHR(SHift Right)`逻辑右移
    + `SAR(Shift Arithmetic Left)`算术左移
    + `SAL(Shift Arithmetic Right)`算术右移

> ⚠️ ❗ 目前的机器，**`SAL`与`SHL`执行效果一样**

<!-- slide -->
# 位移运算符

初始有符号数： 10101010    
左移一位： <font class="bold_and_blue">0</font>101010<font class="bold_and_red">0</font>
逻辑右移一位： <font class="bold_and_blue">0</font>1010101
算术右移一位： <font class="bold_and_blue">1</font>1010101


<!-- slide -->
# 位移运算符

在C语言中

+ `unsigned`，算术位移与逻辑位移结果一样
+ `signed`，左移采用逻辑左移，右移与平台相关（大部分采用算术右移）

```c{.line-numbers, highlight=2-4}
signed char signed_number = 0xAA;//10101010
printf("Signed Shift: %d,%d", \
signed_number<<1, signed_number>>1);
//in MacOS the output will be: 84,-43 ==> 01010100,11010101
```

<!-- slide -->
# 位移运算符

>Right shifting an unsigned quantity always fills the vacated bits with zero. 
>Right shifting a signed quantity will fill with bit signs (“arithmetic shift”) **on some machines** and with 0-bits (“logical shift”) **on others**.”
>
>-- K&R, The C Programming Language

<!-- slide -->

# 位移运算符 - 使用建议

## 位移运算符，建议只使用无符号整型作为操作数

1. `unsigned`： `<<`相当于乘2，`>>`相当于整除2
1. `signed`： 
    + `<<`执行逻辑左移，可能出现符号位变化
    + `<<`少量的平台采用循环左移，**不可移植**
    + `>>`与平台相关，**不可移植**
1. 当位移的位数大于变量的位数时，`GCC`实际位移数为余数，即`i<<33`或`i>>33`时，假设`sizeof(i)==32`，实际位移数为1