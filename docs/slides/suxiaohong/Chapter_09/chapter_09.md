---
presentation:
    theme: beige.css
    minScale: 0.1
    maxScale: 1.5
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom 
    transitionSpeed: default
---

<!-- slide -->
# 第09章：指针Pointer

<!-- slide -->
# 本章学习内容

1. 指针数据类型
1. 指针变量的定义和初始化
1. 取地址运算符，间接寻址运算符
1. 按值调用与按地址调用，指针变量作函数参数
1. 函数指针

<!-- slide -->
# 例7.9未解决的问题：两数交换

<!-- slide -->
# 变量的内存地址

1. 内存是计算机内的存储部件
1. 运行时，所有指令和数据都保存在内存里
1. 编译或函数调用时为变量分配内存单元
1. 按字节编址： 内存的每个字节都有唯一的地址
1. 按类型分配： 按数据类型分配相应长度的内存空间
1. 无符号整型： 内存地址是一个无符号整型（一般用16进制表示）

<!-- slide -->
# 变量的基本要素

1. 变量的名称： 即变量标识符，是内存空间的别名alias，程序本身不为变量名分配内存空间，变量名是高级语言的概念，目标代码中使用内存地址，**C编译器将变量名存储在符号表中**
1. 变量的地址： 变量所占内存空间的地址

<!-- slide -->
# 变量的基本要素（续）

1. 变量的类型： 信息=位+上下文，数据类型是上下文
1. 变量的内容： 即变量所占的内存空间中存储的数据

<!-- slide -->
# C语言的寻址方式

1. 直接寻址： 按变量地址访问变量内容
1. 间接寻址： 通过保存变量B的内存地址的变量A访问变量B的内容

<!-- slide -->
# 再看`scanf`

```c{.line-numbers}
int i;
char c;
scanf("%d",i);  //将i的值当做地址
scanf("%d",&c); //c的内存不足以存放int，其后的空间也被写入数值
```

<!-- slide -->
# 指针

1. 指针Pointer变量是一种用于存放内存地址的变量
1. 变量的指针 is 变量的地址

![pointer_and_variable](./00_Images/pointer_and_variable.png)

<!-- slide -->
# 指针变量的定义和初始化

```c{.line-numbers}
<data_type> *<pointer_name_01>=NULL,\
    *<pointer_name_02>=NULL;
```

## 指针变量指向的数据类型称为基类型

<!-- slide -->
# 指针的强制类型转换

```c{.line-numbers}
    char * pc;
    int i=97;
    pc=(char *)&i;
    printf("%c",*pc);
```

:question::book:P230指针变量只能指向同一基类型的变量

<!-- slide -->
# NULL:book:P229

## NULL是在`<stdio.h>`中定义为零值的宏

<!-- slide -->
# 取址、指针类型和解引用

```c{.line-numbers}
int a=0;
int *pa=NULL;//指针类型
pa=&a;//取址，对指针变量赋值（指针左值）
*pa=9;//解引用，对指针指向的变量赋值（变量左值）
```

## 引用指针所指向的变量的值称为指针的解引用（Pointer Dereference）

<!-- slide -->
# 声明、定义和初始化

1. 声明Declare： 告诉编译器在其他地方有定义，不分配内存空间
1. 定义Define： 告诉编译器分配内存空间（代码段或数据段）
1. 初始化initialized： 第一次赋值

:warning:不同编程语言对声明、定义、初始化的解释略有不同（比如C++）

<!-- slide -->
# 9.4 值传递和引用传递📖P234

1. 值传递Pass by Value/按值调用Call by Value： 实参和形参是两个不同内存地址的变量，函数调用时将实参的值复制给形参，**形参值的改变不影响实参**
1. 引用传递Pass by Reference/按引用调用Call by Reference： 实参和形参指向同一个内存地址，**形参值的改变同时改变实参的值**

<!-- slide -->
# 指针传参

❗️ C语言只有值传递，**教材** 称为 **模拟引用调用Simulating Call by Reference**

<!-- slide -->
# 指针传参（续）

❗️ 指针传参时，实参和形参是 **指向同一内存地址** 的 **两个不同的** 指针变量

![pass_by_pointer](./00_Images/pass_by_pointer.png)

<!-- slide -->
# 9.5 用指针变量作函数参数的程序实例

# :book: P241 - P242 函数入口参数和出口参数

```c{.line-numbers}
//line 21
/**
@param[in]
@param[out]
@return
*/
void FindMax(int score[], long num[], int n, \
                int *pMaxScore, long *pMaxNum)
```

<!-- slide -->
# 9.6 函数指针📖P242

1. "Function Pointer" is "Pointer to a Function"
1. 运行时，函数存储在内存中
1. 函数首条指令的地址即函数入口地址
1. 函数指针指向函数入口地址
1. 函数名即函数入口地址

<!-- slide -->
# 函数指针的意义

1. 编写通用性更强的函数，常用于回调函数
1. 面对对象成员函数的底层实现机制

<!-- slide -->
# 面向对象的底层实现（C++对象模型）

![cover_inside.the.c++.object.model](./00_Images/cover_inside.the.c++.object.model.jpg)

<!-- slide -->
# 面向对象的底层实现（GTK对象模型）

>GTK使用 **C语言开发**，但是其设计者 **使用面向对象技术**。也提供了C++（gtkmm）、Perl、Ruby、Java和Python（PyGTK）绑定，其他的绑定有Ada、D、Haskell、PHP和所有的.NET编程语言。
>
>--Wikipedia

<!-- slide -->
# 函数指针的定义和调用

:book: P248

```c{.line-numbers}
<ret_type> (* func_pointer_name)(<para_list...>);
```

```c{.line-numbers}
(* func_pointer_name)(<argu_list...>);
func_pointer_name(<argu_list...>);
```

<!-- slide -->
# 📖P242：例9.8 && 📖P245：例9.9

:book: P243（L54-L80） ==> :book: P247（L55-L77）

:book: P243（L19-L28） ==> :book: P246（L20-L31）

<!-- slide -->
# 指针变量与普通变量的共性

1. 占据一定大小的内存空间
1. 先定义后使用
1. 明确的数据类型或指向明确的数据类型，C语言是强类型语言

<!-- slide -->
# 指针变量的特殊性

1. 指针变量是一种用于存储内存地址的特殊数据类型（内存地址是 **`unsigned long`** :question:）
1. 指针变量指向已分配的内存空间后才能被访问（野指针访问是未定义行为）
1. 可施加的运算： 加、减、自增、自减、关系、赋值、取址、解引用

<!-- slide -->
# 使用指针的基本原则

1. 明确指针指向的内存地址（位）
1. 明确指针指向的数据类型（上下文）

## 信息 = 位 + 上下文

<!-- slide -->
# 指针声明的优先级规则

```c{.line-numbers}
int *p();//declare a function "p", "p" return a "int *"
int (*p);//define a pointer "p", "p" point to "int"
int *p;//define a pointer "p", "p" point to "int"
int (*p)();//define a function pointer "p"
```

<!-- slide -->
# 指针声明的优先级规则（续01）

## 《C专家编程》:book: 3.3 P64

1. 声明从它的名字开始读取，然后按优先级依次读取
1. 优先级从高到低依次是：
    1. 被括号括起来的那部分
    1. 后缀操作符：`()`表示函数，`[]`表示数组
    1. 前缀操作符：`*`表示指针
1. `const`紧跟类型说明符时作用于类型说明符，其他情况，作用于位于它左边紧邻的`*`

```c{.line-numbers}
char * const * (*next)();
```

<!-- slide -->
# 指针声明的优先级规则（续02）

1. next是一个指针，它指向一个函数 ==> 函数指针
1. 该函数返回另一个指针 ==> 指向指针的指针
1. 函数返回的指针指向一个指针常量（const pointer）
1. 该指针常量指向`char`型变量

<!-- slide -->
# 常量指针、指针常量和指向常量的常量指针 :book: P271

```c{.line-numbers}
const char * p;//常量指针（pointer point to const）
char const * p;//常量指针（pointer point to const）
char * const p;//指针常量（const pointer）
//指向常量的指针常量（const pointer point to const）
const char * const p;
```

<!-- slide -->
# const类型限定符 :book: P271

```c{.line-numbers}
int a, b;
const int *p = &a;
a = 100;//OK
*p = 101;//error
```

<!-- slide -->
# Thank You && To Be Continue...

## End of This Chapter