---
presentation:
    theme: beige.css
    minScale: 0.1
    maxScale: 1.5
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom 
    transitionSpeed: slow 
---

<!-- slide -->
# 第七章： 函数

<!-- slide -->

# 本章内容

## 自顶向下、逐步求精

1. 函数定义、函数调用、函数原型、函数的参数传递与返回值
1. 递归函数和函数的递归调用
1. 函数封装，函数复用，函数设计的基本原则，程序的健壮性
1. 变量的作用域与存储类型：全局变量、自动变量、静态变量、寄存器变量
1. “自顶向下、逐步求精”的模块化程序设计方法

<!-- slide -->

# 数学上的函数与编程上的函数

$y=f(x)$，$y$因变量，$f$函数名，$x$自变量

程序设计中的函数不局限于 **计算类**，还包括 **逻辑类**

<!-- slide -->
# Divide and Conquer❓

1. 较大的任务分解成若干个较小、较简单的任务 $\Rightarrow$ **分解Break Down**
1. 提炼公用任务 $\Rightarrow$ **归纳Induce** $\Rightarrow$ **抽象Abstract**

<!-- slide -->

# Divide and Conquer, Wirth, 1971❓

```{.line-numbers}
Sub-goals are identified by restating the problem goal \
    in term of sub-problem goals.
The classic divide-and-conquer approach with \
    step-wise refinement is one common method for \
    identifying and integrating sub-goals (Wirth 1971).

- "Strategic Software Engineering: \
    An Interdisciplinary Approach" \
    Section6.2 "What is a Problem?"
```

> **逐步求精Step-wise Refinement**

<!-- slide -->

# 逐步求精Step-wise Refinement

```{.line-numbers}
we talked about the idea of "top down stepwise refinement" \
    as a problem-solving technique. This can help a person \
    "think through" an algorithm by doing the following:
1. Start with the initial problem statement
2. Break it into a few general steps
3. Take each "step", and break it further \
    into more detailed steps
4. Keep repeating the process on each "step", \
    until you get a breakdown that is pretty specific, \
    and can be written more or less in pseudocode
5. Translate the pseudocode into real code

- Stepwise Refinement (Example) \
    https://www.cs.fsu.edu/~myers/c++/notes/stepwise.html
```

<!-- slide -->

# Divide and Conquer分治法

```{.line-numbers}
分治原理：要解决规模为n的问题，可递归地解决两个规模近似为n/2的子问题，\
    然后对它们的答案进行合并以得到整个问题的答案

-《编程珠玑（第2版.修订版），section8.3分治算法
```

```{.line-numbers}
分治策略：将原问题划分成n个规模较小而结构与原问题相似的子问题；\
    递归地解决这些子问题，然后再合并其结果，就得到原问题的解。

- 《算法导论（第2版）》，Section2.3.1分治法
```

[五大常用算法之一：分治算法](https://www.cnblogs.com/steven_oyj/archive/2010/05/22/1741370.html)

<!-- slide -->
# 模块化程序设计

## 📖 P146

> + 在结构构化程序设计中，**主要采用功能分解** 的方法来实现模块化程序设计
> + **功能分解** 是一个 **自顶向下、逐步求精** 的过程，即一步一步地把大功能分解成小功能，从上至下，逐步求精，各个击破，直到完成最终的程序。

<!-- slide -->

# 为什么需要函数

## 函数是C语言中模块化程序设计的最小单位 📖

**语句** $\Rightarrow$ **函数** $\Rightarrow$ **文件** $\Rightarrow$ **程序** $\Rightarrow$ **软件**

1. **封装Encapsulation：** 隐藏实现细节，对外公开接口
    * **复杂度控制Complexity control：** 避免单个单元过于复杂
    * **信息隐藏Information Hiding：** 外部不需要知道具体实现细节，只需要知道接口规范

<!-- slide -->
# 为什么需要函数（续）

1. **抽象Abstraction：** 从具体事物抽出、概括出它们共同的方面、本质属性与关系
1. **复用Reuse：** 不要 **重新发明** 轮子（Don't **Reinventing** the wheel）
1. **分工协作Co-operation：** 分解成多个不同的工作单元

> 面向对象三个基本特征（**封装**、**继承**、**多态**）是再进一步的 **抽象**

<!-- slide -->
# 函数的分类

> `main()`由系统执行调用，程序的执行从`main()`的入口开始，到`main()`的出口结束

1. **标准库函数Standard Library Functions**
1. **第三方库函数Three-part Functions**
1. **自定义函数Self-defined Functions**

脚本语言（解释型语言）一般还有 **内置函数Built-in Functions**

<!-- slide -->
# 函数定义

```c{.line-numbers}
<ret_type> <func_name>(<type> <parameter_01>,<...>){
    <declarations>;
    <statements>;
    ...
    return <ret>;
}
```

<!-- slide -->
# 函数体、形参和返回值

1. 函数体： 以`{}`为定界符
1. 形式参数Parameter： 相当于运算的操作数或数学函数的自变量
1. 返回值： `return`语句，相当于运算的结果数或数学函数的因变量

> + 📖 P149： 无任何值返回的`return`语句写成`return ;`
> + 📖 P149： 良好的程序设计风格要求即使无返回值也用`return ;`作为最后一条语句

<!-- slide -->
# 函数调用

1. 函数调用Function Call：
    1. 主调函数
    1. 被调函数
1. 实际参数Argument： 实际传递给函数的数据

> 📖 P149 函数返回值的类型可以是 **除数组以外** 的任何类型 ➡️ 可以是 **指向数组的指针**

<!-- slide -->
# 栈内存与堆内存

1. 函数调用过程通常使用堆栈实现，每个用户态进程对应一个调用栈结构(call stack)
1. 编译器使用堆栈传递函数参数、保存返回地址、临时保存寄存器原有值(即函数调用的上下文)以备恢复以及存储本地局部变量

<!-- slide -->
# 函数声明Function declarations（函数原型Function Prototype）

```c{.line-numbers}
//函数声明末尾有分号结尾
//函数声明形参名可省略
//函数声明形参类型也可省略（Xcode不允许省略），但强烈不推荐
<ret_type> <func_name>(<type> <parameter_01>,<...>);
```

## 为什么需要函数声明❔ 📖 P151

## C语言代码自上往下依次执行，函数声明告诉编译器有这样一个函数

<!-- slide -->
# 防御性程序设计与健壮性

1. 在函数入口增加对函数参数合法性的检查是增强程序健壮性的常用方法
1. 形参和实参类型不匹配时，发生自动类型转换，无法进行自动类型转换时编译器报错

> ⚠️ 📖 部分编译器将`printf()`函数的返回值作为未明确返回时返回主调函数

<!-- slide -->
# 函数设计的基本原则

1. 小巧： 控制在50行以内（每行80字）
1. 功能单一： 只做一件事
1. 接口清晰
1. 入口检查
1. 敏感操作合法性检查

<!-- slide -->
# 函数设计的基本原则（续）

1. 调用其它函数时检查被调函数返回状态并进行处理（ 📖 第7点 + 第8点 ）
1. 返回函数运行状态（正常返回或异常返回）
1. 所有出口的返回值类型都应与函数声明的返回值类型一致
1. 单入口单出口

**注：** “返回函数运行状态”在有“异常”语法的语言中可以使用“异常”

<!-- slide -->

# 递归

1. **递归：** 一个对象部分地由它自己组成或由它自己定义
1. **递归可行的基本原理**
    + 一般情况（General Case）： **分解为与原问题相似** 且 **更简单** 的问题，然后 **持续递归分解**
    + 基线情况（Base Case）： **最简单的问题可以被解决** ，用于 **结束递归调用** 的条件

<!-- slide -->
# 递归调用与递归函数

1. 递归调用Recursive Call： 直接或间接调用自己
1. 递归函数Recursive Function： 直接或间接调用自己的函数

<!-- slide -->
# 递归函数

## 递归函数应该能够在有限次数内终止递归

+ 递归调用若不加以限制，将无限循环调用
+ 在函数内部加控制语句，当满足一定条件时，递归终止，称为条件递归

<!-- slide -->
# 递归函数（续）

## 递归函数应包括两部分

+ 递归循环继续的过程
+ 递归调用结束的过程

```c{.line-numbers}
if(<递归结束条件成立>)｛
    return <递归基线值>;
｝else{
    return <下一步递归分解>;
}
```

<!-- slide -->
# 递归与循环

⚠️ 一般情况下，迭代与循环不同

1. 递归更遵循数学公式的表示： 可读性好
1. 递归需较多的函数调用资源： 可能导致栈溢出
1. 循环可转换成等价的递归，反之则不一定

<!-- slide -->
# 变量的作用域与存储类型

1. 作用域Scope，即：在哪个范围内可以被定位和访问
1. 存储类型Storage Class，即：占据空间的位置及其生存期（分配空间与释放空间）

<!-- slide -->
# 变量的作用域

1. 全局变量Global Variable： 在函数外定义的变量
1. 局部变量Local Variable： 在函数内定义的变量

<!-- slide -->
# 变量的存储类型

1. 自动变量Automatic Variable： `auto`，缺省的存储类型
1. 静态变量Static Variable： `static`
1. 外部变量Extern Variable： `extern`
1. 寄存器变量Register Variable： `register`，一般无须特别注明

<!-- slide -->
# 变量的存储位置

![memory-storage-in-c](./00_Images/memory-storage-in-c.jpg)

## `text segment`又分为`code`和`Literal`

<!-- slide -->
# 变量的作用域 + 存储位置

|--|auto|static|extern|register|
|:--:|:--:|:--:|:--:|:--:|
|Global|--|静态全局变量|全局变量（仅用于声明）|--|
|Local|自动局部变量|静态局部变量|--|寄存器变量|

<!-- slide -->
# 变量的作用域 + 存储位置（续）

![Storage-Classes-In-C](./00_Images/Storage-Classes-In-C.png)

<!-- slide -->
# 全局变量 - 作用域 + 存储类型

+ 全局变量：
    + 作用域： 整个程序
    + 生存期： `main()`前分配，`main()`后释放
+ 静态全局变量：
    + 作用域： 定义变量的文件
    + 生存期： `main()`前分配，`main()`后释放

<!-- slide -->
# 局部变量 - 作用域 + 存储类型

+ 自动局部变量：
    + 作用域： 代码块内
    + 生存期： 函数入口分配，函数出口释放
+ 寄存器局部变量： 存放于寄存器中
    + 作用域： 代码块内
    + 生存期： 函数入口分配，函数出口释放
+ 静态局部变量：
    + 作用域： 代码块内
    + 生存期： 程序首次进入函数入口分配，`main()`后释放

<!-- slide -->
# 模块化程序设计的基本原则

## 高内聚、低耦合

1. 内聚： 模块内部的联系性
1. 耦合： 模块之间的依赖性

## 接口即模块与外部交互的规范，接口是实现低耦合的重要工具

<!-- slide -->
# 模块化程序设计的经典案例

1. 正面案例： POSIX的管道、POSIX的命令行、`Borland Delphi`、`Borland C++ Builder`
1. 反面案例： `Windows API`、`VS C++`的`MFC`

<!-- slide -->
# 自顶向下与自底向上

1. 自底向上Down-Top： 先编写基础片段，然后不断扩大、补充和升级
1. 自顶向下Top-Down： 先编写整个问题框架，然后不断完成问题各个部分

> 📖 P172 逐步求精技术可以理解为一种由不断自底向上修正所补充的自顶向下的程序设计方法

<!-- slide -->
# `assert()`

```c{.line-numbers}
#ifdef NDEBUG
#define assert(condition) ((void)0)
#else
#define assert(condition) /*implementation defined*/
#endif

The definition of the macro assert \
    depends on another macro, NDEBUG, \
    which is not defined by the standard library.

If NDEBUG is defined as a macro name at the point \
    in the source code where <assert.h> is included, \
    then assert does nothing.
```

<!-- slide -->

# `assert()`

```c{.line-numbers}
If NDEBUG is not defined, \
    then assert checks if its argument \
    (which must have scalar type) compares equal to zero. \
    If it does, assert outputs implementation-specific \
    diagnostic information on the standard error output \
    and calls abort().

The diagnostic information is required to \
    include the text of expression, \
    as well as the values of the standard macros \
    __FILE__, __LINE__, \
    and the predefined variable __func__. (since C99)
```

<!-- slide -->
# `assert()`

## `assert()`对`Debug版`有效，对`Release版`无效（不生成代码）

使用`assert()`的情况：

1. 检查程序中的各种假设的正确性
1. 证实或测试某种不可能发生的状况确实不会发生

<!-- slide -->
# `assert()`

```c{.line-numbers}
#include <stdio.h>
// uncomment to disable assert()
// #define NDEBUG
#include <assert.h>
#include <math.h>
 
int main(void){
    double x = -1.0;
    assert(x >= 0.0);
    printf("sqrt(x) = %f\n", sqrt(x));

    return 0;
}
```
<!-- slide -->

# `assert()`

```c{.line-numbers}
output with NDEBUG not defined:
a.out: main.c:10: main: Assertion 'x >= 0.0' failed.

output with NDEBUG defined:
sqrt(x) = -nan
```

<!-- slide -->
# 代码风格

1. 影响可读性： 团队协作、开源成为主流
1. 影响维护效率： 后期维护升级
1. 出差概率： 良好风格的代码出错概率相对低

<!-- slide -->
## 代码风格建议

1. 代码行
1. 对齐与缩进
1. 空行与空格
1. 长行拆分缩进
1. 代码注释

<!-- slide -->

# 代码风格 -- 标识符命名风格

1. Windows/Camel-case Style（骆驼命名法/驼峰式命名法）
1. Unix-like/Open-source/Underscore Delimited（下划线命名法）
1. CONSTANT（大写命名）
1. PascalCase（首字母大写）/kebab-case（中划线命名）

<!-- slide -->
# POSIX与Open Source

1. Unix分为System V和BSD两大风格
1. Unix-like/UN\*X/\*nix风格类似于System V和BSD
1. POSIX(Portable Operating System Interface)
1. Unix-like is **a kind of** POSIX 
1. Free Software（自由软件） is **a kind of** Open Source Software（开源软件）
1. Free Software要求更严格

<!-- slide -->
# Thank You && To Be Continue...

## End of This Chapter