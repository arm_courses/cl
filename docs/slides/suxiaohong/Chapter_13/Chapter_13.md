---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第13章：文件操作

<!-- slide -->
# 本章主要学习内容

1. 二进制文件和文本文件
1. 文件的打开和关闭
1. 文件的顺序读写与随机读写
1. 标准输入输出及其重定向

<!-- slide -->
# 本章主要知识点

1. 理解二进制文件和文本文本的区别和联系
1. 了解 **`stream`** 概念和 **`FILE`** 数据结构
1. 掌握 **`stdio.h`** 中缓冲文件系统的IO函数
1. 掌握IO函数的IO终止符
1. 掌握 **`EOF`** 文件结束符和文件状态函数
1. 了解文件的随机读写和IO重定向

<!-- slide -->
# Von J. Neumann Architecture

## :exclamation:内存Memory跟外存Storage的功能不同

<!-- slide -->
# I/O设备

+ 输入设备
    1. 键盘、鼠标
    1. 软盘、硬盘、光驱（以文件的形式）
    1. 扫描仪、视频采集卡、电视卡、游戏杆、话筒
+ 输出设备
    1. 显示器、打印机
    1. 软盘、硬盘、 CD/DVD-RW （以文件的形式）
    1. 音箱

## 双工设备越来越多

<!-- slide -->
# 标准输入/输出

1. **`CLI`** 一般都提供标准输入与输出设备
1. 一般情况，标准输入是键盘，标准输出是显示器
1. 操作系统可重定向标准输入与输出，比如让文件作为 **`stdin`** 或 **`stdout`** 或 **`stderr`**
1. 重定向对于程序本身而言是透明的

<!-- slide -->
# 文件file

1. 通常意义下，文件指存储在外存上有名字的一组相关数据的集合，文件是数据在外存中的载体
1. 在C语言（或 **`POSIX`** ）中，IO设备被抽象成文件，即包括磁盘文件、终端显示器或打印机等

<!--  
# 文件的存储

可以建立若干文件目录(directory\folder文件夹)
在目录里保存文件
同一级目录里保存的文件不能同名
对使用者而言，只要知道文件的路径(path, 全目录)和文件名，就能使用该文件

-->

<!-- slide -->
# 13.1二进制文件和文本文件:book:373

<!-- slide -->
# 文件的类型Type of Files

1. 二进制文件： 字节序列
    1. 系统不进行字符编码转换
    1. 将每个数据在内存中的存储形式存储到文件
1. 文本文件（:question:ASCII码文件）： 字符序列
    1. 系统自动进行字符编码转换
    1. 将每个数据对应字符集的字符编码存储到文件

## :point_right:关键区别之一：谁进行编码转换

## :point_right:文本文件是一种特殊的二进制文件

<!-- slide -->
# 文件的类型（续）

![bin_file](./00_Images/bin_file.png)

![txt_file](./00_Images/txt_file.png)

<!-- slide -->
# 文件的类型（再续）

+ 二进制文件
    + 优点： 空间小、速度快、机器友好、保密性好
    + 缺点： 人不可读、通用性差
+ 文本文件
    + 优点： 人可读、通用性好
    + 缺点： 空间大、速度慢、人友好、保密性差

## :warning: 文本文件仅能有效存储对应字符集有定义的且可打印的字符编码，否则可能出现乱码

<!-- slide -->
# 流Stream

1. 一般也称为数据流，或字节流、比特流
1. 可以倒流的流：可通过流控（Flow Control）命令定位到指定位置的流
1. 不可倒流的流： 不能定位到指定位置的流

<!-- slide -->
# Files and Streams

1. 流是文件在运行时的抽象
1. 流屏蔽了IO设备的多样性

<!-- slide -->
# 文件的格式Format of Files

## 数据通常需要按照存入的类型才能有效地读出

1. 公开的文件格式
1. 通用或标准的文件格式
1. 不公开或专用的文件格式

<!-- slide -->
# 缓冲型文件系统:book:P374

1. 系统自动在内存中开辟缓冲区
1. 读写文件时，数据先送到缓冲区
1. 利用文件指针标识文件
1. 缓冲型文件系统中的文件操作也称高级文件操作
1. 具有跨平台和可移植的能力

<!-- slide -->
# 非缓冲型文件系统:book:P374

1. 系统不自动开辟缓冲区，需由程序员另行设定
1. 没有文件指针，使用称为文件号的整数来标识文件

<!-- slide -->
# 缓冲IO

1. “高级”指的是用户层/用户态
1. 缓冲IO中的“缓冲”指的是C语言层面
1. **`stdio.h`** （ **`stderr`** 除外）是缓冲IO，是对OS的封装

```c{.line-numbers}
缓冲IO数据流： 数据 --> C标准库缓存区 --> OS内核缓存区 --> 文件

非缓冲IO数据流： 数据 --> OS内核缓存区 --> 文件
```

<!-- slide -->
# 13.2文件的打开和关闭:book:P374

```c{.line-numbers}
FILE *fopen( const char *filename, const char *mode );
int fclose( FILE *stream );
```

<!-- slide -->
# `fopen()`:book:P375

```c{.line-numbers}
/**
parameter filename: path to file
parameter mode: "r", "w","a","+","b"
return: pointer of file, NULL if failure
*/
FILE *fopen(const char *filename, const char *mode);

FILE *fp = fopen("C:\\CONFIG.SYS", "rw");
```

<!-- slide -->
# `FILE`:book:P375

## `stream`的实现，定义了运行时的相关信息

```c{.line-numbers}
typedef struct{
    short level;          /*缓冲区′满′或′空′的程度*/
    unsigned flags;       /*文件状态标志*/
    char fd;              /*文件描述符*/
    unsigned char hold;   /*如无缓冲区不读字符*/
    short bsize;          /*缓冲区的大小*/
      unsigned char *buffer;/*数据缓冲区的位置*/
      unsigned char *curp;  /*指针当前的指向*/
      unsigned istemp;      /*临时文件指示器*/
    short token;          /*用于有效性检查*/
    }FILE;                   /*在stdio.h文件中定义*/
```

<!-- slide -->
# 文件打开方式:book:P375

1. **`"r"`只读：** 打开一个已存在的文件，读
1. **`"w"`覆盖写：** 新建一个文件，写
1. **`"a"`追加写：** 新建或打开一个文件，并在文件尾追加数据，写
1. **`"r+"`读写：** 打开一个已存在的文件，可读可写
1. **`"w+"`覆盖读写：** 新建一个文件，可读可写
1. **`"a+"`追加读写：** 新建或打开一个文件，并在文件尾追加数据，可读可写

## :warning: `mode`是string，`"w"`和`"a"`的区别

<!-- slide -->
# `b`模式

## :point_right: C语言存在文本打开方式和二进制打开方式的原因：不同的系统对文本文件采用不同的换行符

1. **`CLI`** 用于指示输入结束的字符（:warning:用于指示，但不是 **`EOF`** ）：
    + **`Windows`** 用 **`CTRL+Z`** （26）
    + **`POSIX`** 用 **`CTRL+D`**（4）
1. 换行符：
    + **`Windows`** 用 **`\r\n`**（13，10）
    + **`POSIX`** 用`\n`（10）

```c{.line-numbers}
File access mode flag "b" can optionally be specified to open a file \
    in binary mode. This flag has no effect on POSIX systems, \
    but on Windows it disables special handling of '\n' and '\x1A'.
```

<!-- slide -->
# `b`模式（续）

## :point_right:是否使用`b`模式对POSIX系统无影响

## :point_right:Windows系统不使用`b`模式时，系统自动将Windows的换行符和输入结束符风格转换成POSIX风格

## :white_check_mark:为跨平台兼容性，建议二进制文件使用`b`模式打开，以防在Windows平台运行时进行自动转换，而文本文件不使用`b`模式打开，以进行自动转换

## :warning::exclamation:是否使用`b`模式不是区分文件是否是文本文件

<!-- slide -->
# 二进制文件和文本文件的核心区别

## 是否按字符集进行编码然后再按外存文本文件进行存储（如：文本文件中的字符串没有`\0`结束符）

```dot
digraph txt{
    rankdir=LR;
    node[shape=box];
    内存表示 -> 字符集 -> 对应字符集索引号 -> 文本文件;
}
```

<!-- slide -->
# `EOF`

>[Wikipedia: End-of-file](https://en.wikipedia.org/wiki/End-of-file)
>In computing, end-of-file (commonly abbreviated EOF) is a condition in a computer operating system where no more data can be read from a data source. The data source is usually called a file or stream.
>In the C Standard Library, the character reading functions such as getchar return a value equal to the symbolic value (macro) EOF to indicate that an end-of-file condition has occurred. The actual value of EOF is implementation-dependent (but is commonly -1, such as in glibc) and is distinct from all valid character codes. 

<!-- slide -->
# `EOF`（续）

>https://stackoverflow.com/questions/16677632/what-really-is-eof-for-binary-files-condition-character

1. 现代OS不存储结束字符，使用另外的方式表示文件长度
1. `EOF`是C语言表示文件结束或文件读写错误的数值指示
1. 特定情况（主要是`CLI`）使用结束标记符（不是 `EOF` ）指示结束，当C语言遇到该字符时返回 `EOF`

<!-- slide -->
# `fclose()`:book:P375

```c{.line-numbers}
/**
return 0 if success
*/
int fclose(FILE *fp);
```

1. 操作系统级的文件关闭操作
1. 清空缓冲区、释放文件控制块等系统资源

<!-- slide -->
# 13.3按字符读写文件:book:P376

```c{.line-numbers}
int fgetc(FILE *fp);
int fputc(int c, FILE *fp);
```

<!-- slide -->
# `fgetc()`

```c{.line-numbers}
int fgetc( FILE *stream );

The obtained character on success or EOF on failure.
If the failure has been caused by end-of-file condition, \
    additionally sets the eof indicator (see feof()) on stream. \
    If the failure has been caused by some other error, \
    sets the error indicator (see ferror()) on stream.
```

>`fgetc()`以`unsigned char`的方式读取文件流，扩张成一个整数并返回。即，`fgetc()`从文件流中取一个字节，并在高位添加3个字节的零，成为一个小于256的整数，然后返回。

<!-- slide -->
# `fputc()`

```c{.line-numbers}
int fputc( int ch, FILE *stream );

Writes a character ch to the given output stream stream. \
    putc() may be implemented as a macro and evaluate stream more than once, \
    so the corresponding argument should never be an expression with side effects.
Internally, the character is converted to unsigned char just before being written.
```

>`fputc()` 将整数写入文件流之前, 将整数的高3个字节去掉

<!-- slide -->
# 为什么需要`feof()`?

```c{.line-numbers}
int fgetc( FILE *stream );

The obtained character on success or EOF on failure.
If the failure has been caused by end-of-file condition, \
    additionally sets the eof indicator (see feof()) on stream. \
    If the failure has been caused by some other error, \
    sets the error indicator (see ferror()) on stream.
```

## 需要`feof()`和`ferror()`分别判断是真的`EOF`还是`error`

<!-- slide -->
# 例13.1:book:P376

```c{.line-numbers}
if ((fp = fopen("demo.txt","w")) == NULL){
    printf("Failure to open demo.txt!\n");
    exit(0);
}
ch = getchar();
while (ch != '\n'){
    fputc(ch, fp);
    ch = getchar();
}
fclose(fp);
```

## :warning:判断`fopen()`返回值是否等于`NULL`

<!-- slide -->
# 例13.2:book:P377

```c{.line-numbers}
//line 08
if ((fp = fopen("demo.bin","wb")) == NULL)
{ 
    printf("Failure to open demo.bin!\n");
    exit(0);
}
for (i=0; i<128; i++)
{  	
    fputc(i, fp);
}
fclose(fp);
```

<!-- slide -->
# 例13.2:book:P377（续）

```c{.line-numbers}
//line 18
if ((fp = fopen("demo.bin","rb")) == NULL) {
    printf("Failure to open demo.bin!\n");
    exit(0);
}
while ((ch = fgetc(fp)) != EOF) {
    putchar(ch);
}
fclose(fp);
```

<!-- slide -->
# 例13.3:book:P379

```c{.line-numbers}
//line 24
while ((ch = fgetc(fp)) != EOF){
    if (isprint(ch)){
        printf("%c\t", ch);
    }
    else{
        printf("%d\t", ch);
    }
}
```

<!-- slide -->
# 例13.3（续）:book:P380

```c{.line-numbers}
//line 24
while (!feof(fp)){
    ch = fgetc(fp);
    if (isprint(ch)){
        printf("%c\t", ch);
    }
    else{
        printf("%d\t", ch);
    }
}
```

## 为什么会最后会多输出一个`-1`:question:

<!-- slide -->
# `feof()`:book:P379

```c{.line-numbers}
int feof();

RETURN: 
nonzero value if the end of the stream has been reached, otherwise 0

NOTE: 
This function only reports the stream state as reported \
    by the most recent I/O operation, \
    it does not examine the associated data source. \
    For example, if the most recent I/O was a fgetc, \
    which returned the last byte of a file, feof returns zero. \
    The next fgetc fails and changes the stream state to end-of-file. \
    Only then feof returns non-zero.
```

<!-- slide -->
# `fgetc()`

```c{.line-numbers}
int fgetc( FILE *stream );

RETURN: 
The obtained character on success or EOF on failure.

If the failure has been caused by end-of-file condition, \
    additionally sets the eof indicator (see feof()) on stream.\
    If the failure has been caused by some other error,\
    sets the error indicator (see ferror()) on stream.
```

<!-- slide -->
# 文件状态函数

```c{.line-numbers}
int feof( FILE *stream );
int ferror( FILE *stream );
void clearerr( FILE *stream );
```

<!-- slide -->
# `ferror()`

```c{.line-numbers}
int ferror( FILE *stream );
RETRUN: Nonzero value if the file stream \
    has errors occurred, 0 otherwise
```

<!-- slide -->
# 读写文件中的字符串:book:P381

```c{.line-numbers}
char *fgets( char *str, int count, FILE *stream );
int fputs( const char *str, FILE *stream );
```

<!-- slide -->
# `fgets()`

```c{.line-numbers}
char *fgets( char *str, int count, FILE *stream );

Reads at most count - 1 characters from the given file stream\
    and stores them in the character array pointed to by str.\
    Parsing stops if end-of-file occurs	or a newline character is found, \
    in which case str will contain that newline character.\
    If no errors occur, writes a null character\
    at the position immediately after the last character written to str.
```

<!-- slide -->
# `fputs()`

```c{.line-numbers}
int fputs( const char *str, FILE *stream );

Writes every character from the null-terminated string str \
    to the output stream stream, as if by repeatedly executing fputc.

The terminating null character from str is not written.
```

## :point_right: `\0`是C在内存中对字符串的规则

<!-- slide -->
# 例13.4:book:P381

```c{.line-numbers}
//line 8
if ((fp = fopen("demo.txt","a")) == NULL){ 
    printf("Failure to open demo.txt!\n");
    exit(0);
}
gets(str);
fputs(str, fp);
fclose(fp);
```

<!-- slide -->
# 例13.4（续）:book:P381

```c{.line-numbers}
if ((fp = fopen("demo.txt","r")) == NULL){ 
    printf("Failure to open demo.txt!\n");
    exit(0);
}
fgets(str, N, fp);
puts(str);
fclose(fp);
```

<!-- slide -->
# 13.4按格式化读写文件📖P382

```c{.line-numbers}
int fscanf(FILE *fp,const char *format,...);
int fprintf(FILE *fp,const char *format,...);
```

<!-- slide -->
# 例13.5:book:P383

```c{.line-numbers}
//line 71
if((fp=fopen("score.txt","w"))==NULL)

//line 77
for (i=0; i<n; i++){
    fprintf(fp, "%10ld%8s%3c%6d/%02d/%02d", \
        stu[i].studentID, stu[i].studentName,\
        stu[i].studentSex,stu[i].birthday.year,\
        stu[i].birthday.month,stu[i].birthday.day);
    for (j=0; j<m; j++){
        fprintf(fp, "%4d", stu[i].score[j]);
    }
    fprintf(fp, "%6.1f\n", stu[i].aver);
}
```

## :warning: 注意文件的打开模式和写入的数据

<!-- slide -->
# 例13.6:book:P386

```c{.line-numbers}
//line 34
if((fp=fopen("score.txt","r"))==NULL)

//line 39
fscanf(fp,"%d\t%d",n,m);//ugly
for (i = 0;i < *n; i++){
    fscanf(fp, "%10ld", &stu[i].studentID);
    fscanf(fp, "%8s", stu[i].studentName);
    fscanf(fp, " %c", &stu[i].studentSex);//ugly
    fscanf(fp, "%6d/%2d/%2d", \
        &stu[i].birthday.year, &stu[i].birthday.month,
        &stu[i].birthday.day);
    for (j=0; j<m; j++){
        fscanf(fp, "%4d", &stu[i].score[j]);
    }
    fscanf(fp, "%f", &stu[i].aver);
}
```

<!-- slide -->
# 13.5按数据块读写文件:book:P388

```c{.line-numbers}
size_t fread(void *buffer, size_t size, size_t count, FILE *stream);
size_t fwrite(const void *buffer, size_t size, size_t count, FILE *stream);
```

## :warning: 注意`buffer`的数据类型是`void *`

<!-- slide -->
# 例13.7:book:P388

```c{.line-numbers}
//line 79
fwrite(stu, sizeof(STUDENT), n, fp);
//line 92
for (i=0; !feof(fp); i++){
    fread(&stu[i], sizeof(STUDENT), 1, fp);
}
```

>:question::book:P392 :x:
>可能 **因发生字符转换** 而出现莫名其妙的结果，所以这两个函数通常用于二进制文件的输入/输出

<!-- slide -->
# 13.6.1文件的随机读写

1. 随机访问Random Access可指定位到指定位置并进行读写的访问方式
1. 文件位置指针File Location Pointer（文件偏移）指向文件当前读写的位置

```c{.line-numbers}
long ftell( FILE *stream );
//origin: SEEK_SET/0, SEEK_CUR/1, SEEK_END/2
int fseek(FILE *stream, long offset, int origin);
void rewind(FILE *stream);
```

<!-- slide -->
# 缓冲区控制函数

```c{.line-numbers}
int fflush(FILE *stream);//for input, undefined behavior
```

<!-- slide -->
# 13.6.2 标准输入/输出

1. **`stdin/0`：** 标准输入流
1. **`stdout/1`：** 标准输出流
1. **`stderr/2`：** 标准错误流

<!-- slide -->
# 重定向

1. `<`： 输入重定向
1. `>`： 输出重定向
1. `>>`： 追加输出重定向

<!-- slide -->
# Thank You

## End of This Chapter && This Course
