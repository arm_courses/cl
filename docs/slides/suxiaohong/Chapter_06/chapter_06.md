---
presentation:
    theme: beige.css
        # "beige.css"
        # "black.css"
        # "blood.css"
        # "league.css"
        # "moon.css"
        # "night.css"
        # "serif.css"
        # "simple.css"
        # "sky.css"
        # "solarized.css"
        # "white.css"
        # "none.css"  
    minScale: 0.1
    maxScale: 1.5
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom # none/fade/slide/convex/concave/zoom
    transitionSpeed: defalut # default/fast/slow
---

<!-- slide -->
# 第六章： 循环控制结构

## 广东财经大学华商学院信息工程系

### aRoming

<!-- slide -->
# 为什么需要循环结构

1. 有很多任务需要重复执行
1. 有很多任务可以建模成重复执行
1. 结构化编程（甚至是命令式编程）的基本结构： **顺序结构**、**分支结构**、**循环结构**

备注： 循环结构也可以分解成分支结构+跳转结构，而分支结构本身也是一种跳转结构

<!-- slide -->
# 循环的类型：“当型”与“直到型”

1. 当型循环（`while loop`）： 条件假时跳出循环体
2. 直到型循环（`until loop`）： 先执行循环体，直到条件为假时跳出循环体❓

<!-- slide -->
# 循环的类型：“当型”与“直到型”

>`While Loop` execute the block of code when the condition is true and keep executing that till the condition becomes false. **Once the condition becomes false**, the while loop is terminated.
>
>`Until Loop` execute the block of code when the condition is false and keep executing that till the condition becomes true. **Once the condition becomes true**, the until loop is terminated.

<!-- slide -->
# 循环的类型：“当型”与“直到型”

```plantuml
@startuml
title __**While Loop**__
start

while(<condition>?) is (**true**)
    :some jobs;
endwhile(**false**)

stop
@enduml
```

```plantuml
@startuml
title __**Until Loop**__

start

repeat
    :some jobs;

repeat while(<condition>?) is (**false**)
-[#red]-> **true**;


stop
@enduml
```

<!-- slide -->
# 循环的类型：“当型”与“直到型”

```bash{.line-numbers, highlight=2}
COUNTER=0
while [ $COUNTER -lt 10 ]; do
    echo The counter is $COUNTER
    let COUNTER+=1 
done
```
```bash{.line-numbers, highlight=2}
COUNTER=20
until [ $COUNTER -lt 10 ]; do
    echo The counter is $COUNTER
    let COUNTER-=1
done
```

<!-- slide -->
# 计数循环 VS 条件循环

1. 计数循环（Counter Controlled Loop）： 
    + **判断计数器是否为特定值** 从而决定是否执行循环
    + :book: **循环次数事先已知** 的循环
1. 条件循环（Condition Controlled Loop）：
    + **判断条件是否满足** 从而决定是否执行循环
    + :book: **循环次数事先未知的由条件控制** 的循环

<!-- slide -->

# 标记循环

1. 标记循环（Sentinel Controlled Loop）：也称为“哨兵循环”
    + **判断标记（或称“哨兵”）是否是一个约定的特殊值** 从而决定是否执行循环
    + 主要是一种 **循环次数未知** 的循环
1. 标记循环可以看成是一种特殊的条件循环

<!-- slide -->
# Loop VS Iterate VS Traversal VS Recursion
1. 循环（loop）： 最普遍的概念，所有重复的行为
1. 迭代（iterate）
    + 数学上的迭代： 在多次重复中逐步接近结果
    + 编程上的迭代： 按一定规则访问线性结构的各项
1. 遍历（traversal）： 按一定规则访问非线性结构的各项
1. 递归(recursion) ： 在函数内重复调用自身

<!-- slide -->
# C语言的循环语句
**`C`只提供当型循环**
1. `while`
    + `while`
    + `do-while`
1. `for`

<!-- slide -->
# `while` VS `do-while`
```c{.line-numbers}
while(<condition>){//Body of Loop
    <statement_01>;
    <statement_02>;
    ......
}

<outside_statement>;
```

```c{.line-numbers,highlight=5}
do{//Body of Loop
    <statement_01>;
    <statement_02>;
    ......
}while(<condition>);//Here has the semicolon...

<outside_statement>;
```

<!-- slide -->
# `while` VS `do-while`
```plantuml
@startuml
title __**while loop**__
start

while(<condition>?) is (**true**)
    :some jobs;
endwhile(**false**)

stop
@enduml
```

```plantuml
@startuml
title __**do-while loop**__

start

repeat
    :some jobs;
repeat while(<condition>?) is (**true**)
->**false**;
stop
@enduml
```

<!-- slide -->
# `for`
```c{.line-numbers,highlight=1}
for(<initial>;<condition>;<update>){//Body of Loop
    <statement_01>;
    <statement_02>;
    ......
}

<outside_statement>;
```

<!-- slide -->
# `for` VS `while`
```c{.line-numbers}
<initial>
while(<condition>){
    <statement_01>;
    <statement_02>;
    ......
    <update>;
}
```
```c{.line-numbers}
<initial>
for(;<condition>;){
    <statement_01>;
    <statement_02>;
    ......    
    <update>;
}
```

<!-- slide -->
# `for` VS `while` VS `do-while`
1. `for`与`while`大部分情况可以互转等价，差别在于简洁性和易读性
1. 当`while`的循环体使用`continue`跳过`<update>`时，`for`与`while`不等价
1. `while`与`do-while`当首次判断`condition`时即为`false`的情况下不等价（`while`没有执行循环体，`do-while`执行了一次循环体）

<!-- slide -->
# `for` VS `while` VS `do-while`
1. **`for`：** 循环次数已知、计数控制的循环，一般地，`for`语句编写的计数循环相对于`while`语句编写的计数循环更简洁易懂
1. **`while`：** 循环次数未知、条件控制的循环，一般地，`while`语句编写的条件循环相对于`for`语句编写的条件循环更直观易懂
1. **`do-while`：** 循环体至少需要被执行一次、循环次数未知、条件控制的循环

**“一般原则”，而非“必须原则”，`do-while`使用频率较低（可能因为比较不符合人的逻辑直觉）**


<!-- slide -->
# 逗号运算符和空语句
1. 逗号运算符（Comma Operator）： 从左至右，依次执行表达式，整个表达式的值为最后一个表达式的值
1. 空语句（NULL Statement/Empty Statement）： 什么都不做的表达式

<!-- slide -->
# 小细节
1. 📖 P102✅ ，即使循环体只有一条语句，也使用花括号
1. 📖 P104⚠️ ，从1开始计数 ==> 下标和计数大多数从0开始，教材中为特殊情况
1. 📖 P107⚠️
```c{.line-numbers}
for (i = 1, j = n; i <= j; ++i, --j){
    sum += (i == j ? i : i + j);
}
```

<!-- slide -->
# 嵌套循环
1. 嵌套循环（Nested Loop）： 循环中有另一个循环
1. 时间复杂度：📖 P112 -> $O(n^2)$ ，📖 P113 -> $O(n)$
1. 📖 P114： ❓为避免造成混乱，❗本质是语义问题

<!-- slide -->
# `rand()`
`rand()`： 每次调用返回在伪随机序列中的一个伪随机数，该伪随机数的值分布于`0 ~ RAND_MAX`
```c{.line-numbers}
int rand();

Returns a pseudo-random integer value
between 0 and RAND_MAX (0 and RAND_MAX included).

srand() seeds the pseudo-random number
generator used by rand().
If rand() is used before any calls to srand(), 
rand() behaves as if it was seeded with srand(1). 
Each time rand() is seeded with srand(), 
it must produce the same sequence of values.
```

<!-- slide -->
# `srand()`
`void srand(unsigned seed)`： 根据`seed`生成伪随机数序列（伪随机数组成的数列）
```c{.line-numbers}
void srand( unsigned seed );

Seeds the pseudo-random number generator 
used by rand() with the value seed.

If rand() is used before any calls to srand(), 
rand() behaves as if it was seeded with srand(1).

Each time rand() is seeded with the same seed, 
it must produce the same sequence of values.

srand() is not guaranteed to be thread-safe.
```

<!-- slide -->
# `time()`
```c{.line-numbers}
time_t time( time_t *arg );

Returns the current calendar time encoded as a time_t object,
and also stores it in the time_t object pointed to by arg
(unless arg is a null pointer)
```

```c{.line-numbers}
typedef /* unspecified */ time_t;
Although not defined by the C standard, 
this is almost always an integral value
holding the number of seconds (not counting leap seconds) 
since 00:00, Jan 1 1970 UTC, corresponding to POSIX time
```

📖 P120：❓ 使用`NULL`作为`time()`的参数时，`time()`的返回值被转换为一个无符号整数

<!-- slide -->
# `time() -> srand() -> rand()`
1. **伪随机数种子** 相同的 **伪随机数序列** 也相同
1. `srand()`使用`time()`的返回值作为 **伪随机数种子** 为`rand()`产生 **伪随机数序列**

<!-- slide -->
# 处理异常输入
```c{.line-numbers}
scanf(const char * format, ...);

Number of receiving arguments successfully assigned 
(which may be zero in case a matching failure occurred
before the first receiving argument was assigned),
or EOF if input failure occurs
before the first receiving argument was assigned.
```

<!-- slide -->
# 处理异常输入
```c{.line-numbers}
while(getchar() != '\n') ;//Empty Statement
```
<!-- slide -->
# 流程转移控制语句
1. `goto`： 非结构化跳转语句，可任意方向跳转（当前函数内）， **不再建议使用`goto`语句**
1. `break`： 结构化跳转语句，跳出`switch`分支语句，也可以跳出`while`、`do-while`、`for`循环
1. `continue`： 结构化跳转语句，跳过`continue`之后的语句进入下一次循环，可用于`while`、`do-while`、`for`
1. `return`： 从被调用函数中返回

<!-- slide -->
# `goto`
```c{.line-numbers}
<label>://Here has the colon
<statement_01>;
<statement_02>;
if(<condition>) {goto <label>;}
if(<condition>) {goto <another_label>;}

<statement_XX>;
<another_label>://Here has the colon
...
```
📖 P126
+ ❓ `goto`的作用是不需任何条件直接跳转
+ ❗ 可能更准确的描述是 **非结构化跳转（随意跳转）**

<!-- slide -->
1. `break`： 运行`break`后，**跳出** 循环体或`switch`
1. `continue`： 运行`continue`后，跳过循环体剩余语句，**进入下一次循环** 判断

<!-- slide -->
# `break`/`continue`
```c{.line-numbers}
while(<condition>){
    ...
    if(<condition>) {break;/continue;}
    ...
}
```
📖 P126
+ ❓ `break`是一种有条件的跳转语句
+ ❗ 可能更准确的描述是 **结构化跳转（限定区域跳转）**，`break`只能跳转到包含它的循环或`switch`外的下一条语句，`continue`只能跳转到下一次循环

<!-- slide -->
# 穷举法
穷举（Exhaustion）： 依次尝试所有可能解

📖 P131的逻辑判断可以改写成性能更优的写法
```c{.line-numbers}
while(1){
    if(...){
        find = 1;
        break;
    }    
}
```

<!-- slide -->
# Structured Programming
1. 1965年，E.W.Dijkstra在一次国际会议上首次提出
1. 1966年，C.Bohm和G.Jacopini首先证明了“只用顺序、选择、循环三种基本的控制结构就能实现任何单入口、单出口的程序”， **奠定了结构化程序设计的基础**
1. 1971年，IBM公司的Mills提出“程序应该只有一个入口和一个出口”， **补充了结构化程序设计的规则**

<!-- slide -->
# Structured Programming
**目前未有一个普遍认同的、严格的定义**
1. 1974年，D.Gries教授将已有的对结构化程序设计的不同解释归纳为13种
1. 一个比较流行的定义
    + 结构化程序设计是一种进行程序设计的原则和方法
    + 避免使用goto语句
    + 采用“自顶向下、逐步求精”方法进行程序设计

**结构清晰、容易阅读、容易修改、容易验证**

<!-- slide -->
# 类型溢出
1. 数据溢出（Data Overflow）： 需要被表示的数值超出类型所能表示的 **数值范围** 产生的数据截断，一般指整型溢出（Integer Overflow）
1. 精度丢失（Precision Loss）： 需要被表示的精度超出类型所能表示的 **精度范围** 产生的数据截断，一般指浮点数的精度丢失（Loss of Significant）

📖 P135 “例6.15”属于哪种类型❓

<!-- slide -->
# 常量 VS 字面量
1. 常量（Constant）： 初始化后不可写的变量
1. 字面量（Literal）： 词法上的概念（lexical conventions），指源程序中表示固定值的符号（token）， **字面量自始至终只能作为右值**
```c{.line-numbers}
const int i = 10;//i是常量，10是字面量
```

👉 **常量和字面量能不能进行取址操作？**

<!-- slide -->
# C语言的字面量
1. 数值字面量
    + 整型字面量
    + 浮点型字面量
1. 字符字面量
    + 字符型字面量
    + 字符串字面量
1. 枚举字面量： 类型为`int`，而非`enum`
1. 指针字面量： `NULL`

<!-- slide -->
# 数值字面量
数值字面量不包含正负号
+ 整型字面量： 整型字面量转化为整型类型时，规则比较复杂
+ 浮点型字面量： 无后缀时为`double`，后缀为`f/F`为`float`，后缀为`l/L`为`long double`

<!-- slide -->
# 整型字面量隐式转化
整型字面量隐式转化为整型时，按下述顺序转化：

`int -> unsigned -> long -> unsigned long -> long long -> unsigned long long`

1. 10进制表示的字面量仅考虑有符号整型
1. 8进制或16进制的字面量先考虑能否用有符号整型，如不能，再考虑能否用同样长度的无符号整型

📝 字面量0x87654321，是一个正值，用4字节的`int`无法表示，编译器会选用`unsigned int`来表示

<!-- slide -->
# 整形字面量显式后缀
1. `l/L`： `long`
1. `ll/LL`： `long long`
1. `u/U`： `unsigned`

❗ 无后缀时进行隐式转化
⚠️ ⚠️ ⚠️  **坑多，路上小心** ❗❗❗

<!-- slide -->
# 字符型字面量
+ C的字符型字面量为`int`（C++为`char`）
+ ` u8/L/u/U`： 分别代表`utf-8/wchar_t/char16_t/char32_t`
+ `utf-8/wchar_t/char16_t/char32_t`是`typedef`
```cpp{.line-numbers}
//In C the assert will success
//In C++ the assert will fail
assert(sizeof('a') == sizeof(int));
```
⚠️ ❗ **字面量的类型 != 对应变量的类型**

<!-- slide -->
# 字符串字面量
+ 无名数组指针`char *`（C++为`const char *`）
+ 包含额外的`\0`
+ 修改无名数组中的元素的结果未定义

<!-- slide -->
# 字符串字面量 - 三字符组替换
为支持老的代码，C语言规定了预处理的三字符组替换（以`??`开始的三字符）
1. 从`VC++ 2010`开始默认不自动替换三字符组，如果需要三字符组替换（如为了兼容古老的软件代码），需要设置编译器命令行选项`/Zc:trigraphs`
1. `GCC`默认不识别三字符组，但会给出编译警告，在指定了C/C++标准时，`GCC`才会识别三字符组，但仍会给出编译警告


<!-- slide -->
# Debug的方法
1. 定位可能的区域
1. 精心思考、仔细审视
1. 查看内存状态

<!-- slide -->
# Debug的工具
1. `Debugger`，如`GDB`
1. `assert()`

<!-- slide -->
# End of This Chapter

## Thank You...

## &&

## To Be Continue...