---
    presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->

# 第12章：结构体与共用体:book: P331

<!-- slide -->
# 本章学习内容

1. 结构体，共用体、枚举、数据类型别名
1. 结构体变量、结构体数组、结构体指针
1. 结构体成员引用、成员选择运算符、指向运算符
1. 传递结构体变量、结构体数组、结构体指针
1. 动态数据结构、单链表

<!-- slide -->
# 12.1 从基本数据类型到抽象数据类型:book:P331

1. 在机器指令集层面，数据均是二进制数
1. 一般情况下，CPU只支持整数和浮点数两种类型

<!-- slide -->
# 高级语言对数据类型的扩展

1. 高级语言引入基本数据类型，不同语言定义不同的基本类型
1. 有些语言（如PL/1）中规定较多的高阶数据类型，如数组、树、栈等

<!-- slide -->
# 复合数据类型

1. **复合数据类型（Compound Type）：** 在基本数据类型的基础上复合构造产生的数据类型
1. **抽象数据类型（Abstract Data Type，ADT）：** 数据及其相关操作的封装
1. **类（Class）：** ADT的一种具体实现方式

## :point_right: ADT是逻辑模型，独立于具体实现

<!-- slide -->
# 12.2.1为什么要定义结构体类型:book: P332

## 不使用结构体的情况:book: P333：

1. **性能问题：** 内存分配分散，访问效率低
1. **内聚问题：** 结构零散，局部数据相关性弱
1. **易读问题：** 不易管理，编码时易发生错误

## 逻辑结构与物理结构差异大:point_right: 图12-1 图12-2

<!-- slide -->
# 12.2.2结构体变量的定义:book: P334

## 结构体模板声明

```c{.line-numbers}
struct <struct_name> {
    <data_type> <member_name>;
    <data_type> <member_name>;
    ...
};//don't forget the semicolon
```

## :exclamation: :book: P335 结构体模板声明了一种数据类型，定义了数据成员的组织形式，但并未定义具体的变量

<!-- slide -->
# 定义结构体变量:book: P335

1. 先声明结构体模板，再定义结构体变量
1. 声明结构体模板的同时定义结构体变量

```c{.line-numbers}
struct <struct_name> <var_name>;

//省略<struct_name>时，定义匿名结构体
struct <struct_name>{
    ...
}<var_name>;
```

<!-- slide -->
# 12.2.3用typedef定义数据类型:book: P336

```c{.line-numbers}
typedef int INTEGER;
typedef struct student STUDENT;
typedef struct student{
    ...
}STUDENT;
```

## `typedef`为已存在的类型起别名，未定义新的类型

## :question:别名通常使用大写字母，`size_t, time_t`

<!-- slide -->
# `typedef`

```c{.line-numbers}
typedef struct student_{...} student;
typedef struct student{...} student;
typedef struct {...} student;
```

<!-- slide -->
# 12.2.4结构体变量的初始化:book: P337

```c{.line-numbers}
STUDENT stu1 = {<...>,{<...>}};
```

## 将成员值置于花括号中进行结构体变量的整体赋值

<!-- slide -->
# 12.2.5嵌套的结构体:book:P337

## 嵌套的结构体（Nested Structure）即： 在一个结构体内包含了另一个结构体作为其成员

```c{.line-numbers}
typedef struct date{
    ...
}DATE;
typedef struct student{
    ...
    DATE birthday;
}STUDENT;
```

<!-- slide -->
# 12.2.6结构体变量的引用:book:P339

## :question: 应该是“结构体成员变量的访问”

## 成员选择运算符（圆点运算符），嵌套结构体成员以级联方式访问

```c{.line-numbers}
<struct_var_name>.<member_name>
stu1.birthday.year = 2000;
```

<!-- slide -->
# 结构体变量整体赋值:book:P340

## C语言允许相同的结构体类型变量整体赋值

```c{.line-numbers}
//line 18
STUDENT stu1 = { 100310121, "王刚", 'M', \
    { 1991, 5, 19 }, { 72, 83, 90, 82 } };
STUDENT stu2;
stu2 = stu1;
```

<!-- slide -->
# 结构体成员变量赋值:book:P340

## 结构体成员是否允许直接使用赋值运算符赋值，遵循该成员变量类型的规则，如数组不可直接使用赋值运算符赋值:book:341

<!-- slide -->
# 深复制和浅复制

```c{.line-numbers}
typedef struct{
    int i;
}s_deep;

typedef struct{
    int * pi;
} s_shadow;
```

<!-- slide -->
# 深复制和浅复制（续）

```c{.line-numbers}
    s_deep sd_01={100},sd_02;
    sd_02 = sd_01;//deep copy
    sd_01.i = 300;
    printf("sd_01.i => %d,sd_02.i => %d",\
           sd_01.i,sd_02.i);//300,100
    
    int ani=200;
    s_shadow ss_01={&ani},ss_02;
    ss_02=ss_01;
    *ss_01.pi = 400;
    printf("ss_01.i => %d,ss_02.i => %d",\
           *ss_01.pi,*ss_02.pi);//400,400
```

<!-- slide -->
# 结构体类型的声明位置:book:P341

1. 全局声明： 全局可访问
1. 局部声明： 本函数体外不可访问

```c{.line-numbers}
void demo_struct_in_func(void){
    typedef struct {
        int i;
        char ch;
    }s_in_func;
    s_in_func sif;
}
```

<!-- slide -->
# 12.2.7结构体所占内存的字节数:book:P342

1. 结构体类型的字节数 == sum(成员类型字节数):question:
1. :point_right::book:P343 为提高寻址效率，大多数系统进行内存对齐（Memory-Alignment），与具体系统和编译器相关
1. 32位系统中，**`short`** 从偶数地址存放，**`int`** 对齐到4字节地址边界
1. 少数系统（如 **`DOS`** ）不进行内存对齐

<!-- slide -->
# 12.3结构体数组的定义和初始化:book:P344

## :book:P345例12.3

```c{.line-numbers}
//line 19
STUDENT stu[30]={{<struct_members>},<...>};

//line 29
sum[i] = sum[i] + stu[i].score[j];
```

<!-- slide -->
# 12.4.1 指向结构体变量的指针:book:P346

```c{.line-numbers}
STUDENT *pt = &stu1;
pt -> studentID = 100310121;
(*pt).studentID = 100310121;
pt -> birthday.year = 1991;//->和.同优先级，从左至右结合
```

## :question:很少听过“指向运算符”这个名词，一般用“箭头运算符”或“结构解引用运算符Structure dereference operator”，`->`可以视为`(*).`的语法糖

<!-- slide -->
# 12.4.2 指向结构体数组的指针:book:P347

## :question:应该是“指向结构体数组元素的指针”

```c{.line-numbers}
STUDENT *pt = &stu[0];
```

<!-- slide -->
# 12.5 向函数传递结构体:book:P347

## 复制内容，直观、开销大

1. 向函数传递结构体的单个成员
    1. 复制单个成员的内容
    1. 函数内对结构内容的修改不影响原结构
1. 向函数传递结构体的完整结构
    1. 复制整个结构体成员的内容，多个值
    1. 函数内对结构内容的修改不影响原结构

<!-- slide -->
# 12.5 向函数传递结构体:book:P348

## 复制指针，开销小

1. 向函数传递结构体的首地址
    1. 用结构体数组/结构体指针作函数参数
    1. 仅复制结构体的首地址，一个值
    1. 修改结构体指针所指向的结构体的内容

## 如何限制函数中不能修改指针形参指向的结构体:question:

<!-- slide -->
# 结构体变量作为函数返回值:book:P350

```c{.line-numbers}
\\line 08
struct date Func(struct date p){
    ...
    return p;
}
```

<!-- slide -->
# 12.6 共用体:book:P353

1. 共用体，也称联合（union），将不同类型的数据组织在一起共同占用同一块内存空间的一种构造数据类型
1. 共用体与结构体的类型声明方法类似，关键字不同

<!-- slide -->
# 共用体:book:P354

1. `sizeof(union)`为占空间最多的成员变量
1. 同一内存单元在每一时刻只能存放其中一种类型的成员
1. 起作用的成员是最后一次存放的成员
1. 不能作为函数参数和函数返回值
1. 不能进行比较操作

<!-- slide -->
# 共用体的初始化:book:P355

:warning::book:P355只能对第一个成员初始化

```c{.line-numbers}
typedef union sample_union_{
    int i;
    char ch;
}sample_union;
sample_union su_01={97};//C89
sample_union su_02={.ch = 'b'};
```

:point_right: C99允许指定具体成员进行初始化

<!-- slide -->
# 共用体成员访问

## :warning: 不能直接对共用体变量赋值，也不能直接引用共用体变量得到一个值

## :point_right: 应使用“成员选择运算符”或“箭头运算符”访问具体的成员

<!-- slide -->
# 共用体作为结构体的成员:book:P356

```c{.line-numbers}
//line 24
struct person{
    char name[20];
    char sex;
    int age;
    union maritalState marital;
    int marryFlag;
}
```

## :warning: `marryFlag`用于指示共同体哪个成员变量有效

<!-- slide -->
# 共同体作为不同的维度访问

```c{.line-numbers}
typedef union sample_union_{
    int i;
    unsigned char ch[4];
}sample_union;
```

<!-- slide -->
# 结构体 VS 共用体

||结构体|共用体|
|--|--|--|
|成员关系|相关|互斥或维度不同|
|内存分配|相邻|共用|
|函数参数|可|不可|
|函数返回值|可|不可|

<!-- slide -->
# 数组、结构体和共用体

1. **数组（Array）：** 相同类型的数据组成的一种数据结构，是一种数据的容器
1. **结构体（Structure）：** 不同类型的数据成员组织到统一名称、连续内存
1. **共用体（Union）：** 不同类型的数据成员组织到统一名称、复用内存

<!-- slide -->
# 12.7 枚举数据类型:book:P356

1. 枚举（Enumeration）用于当某些量仅由有限个数据值组成时
1. 算法中的穷举法也称为枚举法

```c{.line-numbers}
enum <enum_name>{<enum_value>, <...>};
enum <enum_name> <enum_varname>;
```

<!-- slide -->
# 枚举类型的使用:book:P357

```c{.line-numbers}
typedef enum response_{
    no = -1, yes = 1, none = 0, unsure =2
} response;

response answer = none;
if(answer == yes){
    ...
}
```

<!-- slide -->
# 枚举标签:book:P357

## 枚举标签Enumeration Tag： 枚举类型

## :exclamation:“枚举标签”这个名词使用场景很少

<!-- slide -->
# 标举常量:book:P357

## 枚举类型是一个集合，集合元素（枚举成员）是有名字的整型常量

1. 枚举常量Enumeration Constant： 枚举成员（**`int`型**），**一般情况下，枚举成员名全大写**
    1. 默认情况： 第一个成员为0，后续成员加1
    1. 明确指定： 未明确指定值的成员在前一成员加1

<!-- slide -->
# 标举常量的访问

## :warning::exclamation:枚举成员的作用域与枚举类型一致，直接访问

## :warning::exclamation:枚举类型不使用成员访问方式，而是直接赋值

<!-- slide -->
# 枚举常量的名称冲突

## :exclamation:不能在交叉的作用域中出现同名的枚举常量

```c{.line-numbers}
typedef enum sample_enum_01_{
    FIRST, SECOND,THIRD
}sample_enum_01;

//error: redefinition of enumerator 'FIRST'
typedef enum sample_enum_02_{
    FIRST, 2ND,3RD
}sample_enum_02;
```

<!-- slide -->
# 枚举常量值

```c{.line-numbers}
typedef enum {NO = -1, YES = 1, NONE = 0;}response;

typedef enum{JAN = 1, FEB, ...}mouth;

typedef enum{A=1,B,C=2,D}e_number;

assert(sizeof(<enum_type>) == sizeof(int));
```

<!-- slide -->
# `enum`与`int`

```c{.line-numbers}
typedef enum season_{
    SPRING, SUMMER, FALL, WINTER
} season;

season s = SPRING;
int i = SPRING;
i = 10;
s = (season)i;//some compiler accept without explicit cast
printf("%d",s);
```

## :exclamation: 保证`int`转`enum`有意义是程序员的责任

<!-- slide -->
# 遍历枚举类型

```c{.line-numbers}
enum DAY{
      MON=1, TUE, WED, THU, FRI, SAT, SUN
} day;
for (day = MON; day <= SUN; ++day) {
        printf("element：%d \n", day);
}
```

## :exclamation:枚举常量不连续的枚举类型无法遍历

<!-- slide -->
# C枚举是`const int`的语法糖

>Although variables of enum types may be declared, **compilers need not check** that what you store in such a variable is a valid value for the enumeration.
>
>-- K&R, The C Programming Language

<!-- slide -->
# ADT

## :point_right:ADT更多情况下是数据结构上的概念

## :point_right:数据结构是存储、组织数据的方式

## :point_right:数据结构是相互之间存在一种或多种特定关系的数据元素的集合

<!-- slide -->
# 12.8 动态数据结构 - 单向链表:book:P358

1. 数组的优点：
    + 使用直观
    + 随机存取元素
1. 数组的缺点：
    + 移动元素成本高
    + 重新分配长度需整体复制

## :question:若想改变，只能修改程序:x:

<!-- slide -->
# 指向本结构体类型的指针成员:book:P358

```c{.line-numbers}
struct temp{
    int data;
    struct temp another_temp;//error
    struct temp * next;//OK
}
```

## :warning:结构体不能包含本结构体类型的成员，但可以包含指向本结构体类型的指针成员

<!-- slide -->
# 链表

1. 链表（Linked Table）是一种线性表的链式存储结构，存储单元可以是连续的，也可是不连续的
1. 链表的优缺点刚好与数组相反
1. 链表可再细分为单链表、双链表、环形链表等

<!-- slide -->
# 链表结构体和操作

+ 12.8.2链表结构体声明
+ 12.8.3链表的建立操作 **`AppendNode()`**
+ 12.8.4链表的删除操作 **`DeleteNode()`**
+ 12.8.5链表的插入操作 **`InsertNode()`**

<!-- slide -->
# 12.8.2 链表的定义

## 为表示每个元素与后继元素的逻辑关系，除存储元素本身信息外，还要存储其直接后继信息

```c{.line-numbers}
struct LinkNode{
    int data;
    struct LinkNode *next;
};
```

## n个节点链接成一个链表（因为只包含一个指针域，只能表示单一方向，故又称线性链表或单向链表）

<!-- slide -->
# 链表的组成:book:P359

1. 头指针head： 访问链表的入口
1. 节点node： 链表的元素
    1. 数据域data
    1. 指针域next
1. 尾节点rear node： **`next`** 为 **`NULL`** 表示链表结尾

<!-- slide -->
# 断链:book:P360

## :warning:链表某个节点的指针域数据丢失，将无法找到后继节点

<!-- slide -->
# 12.8.3 链表的建立:book:P360

## 依次将新节点添加到链尾

1. 若原链表为空表（**`head == NULL`**），则将新建节点p置为头节点
1. 若原链表为非空，则将新建节点p添加到表尾

<!-- slide -->
# `AppendNode()`:book:361

```c{.line-numbers}
//line 40
if(head == NULL){
    head = p;
}else{
    while(pr->next != NULL){
        pr = pr->next;
    }
    pr->next = p;
}
//line 55
p->next = NULL;
```

<!-- slide -->
# 链表的删除

1. 若原链表为空表，则退出程序
1. 若待删除节点p是头节点，则将head指向当前节点的下一个节点即可删除当前节点
1. 若待删除节点不是头节点，则将前一节点的指针域指向当前节点的下一节点即可删除当前节点
1. 若已搜索到表尾（p->next == NULL）仍未找到待删除节点，则显示“未找到”

<!-- slide -->
# `DeleteNode()`:book:P363

```c{.line-numbers}
//line 15
if(nodeData == p->data){
    if(p == head){
        head = p->next;
    }else{
        pr->next = p->next;
    }
    free(p);
    //line 31
    return head;
}
```

<!-- slide -->
# 12.8.5 单向链表的插入操作:book:P364

1. 若原链表为空表，则将新节点p作为头节点，让head指向新节点p
1. 若原链表为非空，则按节点值（假设已按升序排序）的大小确定插入新节点的位置
1. 若在头节点前插入新节点，则将新节点的指针域指向原链表的头节点，且让head指向新节点
1. 若在链表中间插入新节点，则将新节点的指针域指向下一节点且让前一节点的指针域指向新节点
1. 若在表尾插入新节点，则末节点指针域指向新节点

<!-- slide -->
# `InsertNode()`

```c{.line-numbers}
//line 13
if(head == NULL) {head = p;}
else{
    while(pr->data < nodeData && pr-next != NULL){
        temp = pr; pr = pr->next;
    }
    if(pr->data >= nodeData){
        if(pr==head){p->next=head;head=p;}
        else{pr=temp;p->next=pr-next;pr->next=p;}
    }else{pr->next=p;}
}
```

<!-- 
# 链表的遍历

-->

<!-- slide -->
# Thank You && To Be Continue...

## End of This Chapter